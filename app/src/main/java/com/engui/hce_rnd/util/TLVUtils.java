package com.engui.hce_rnd.util;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class TLVUtils {

    static String[] tagList = {"0x9F27", "0x9F1A", "0x9A", /*"0x9F26",*/ "0x82", "0x9F36", "0x9F37",
            "0x95", "0x9C", "0x9F10", "0x5F2A", "0x9F02", "0x5F34", "0x9F35", "0x9F03",
            "0x9F1E", "0x9F33", "0x9F34", "0x9F41", "0x9F08", "0x9F06", "0x9B", "0x9F6E",
            "0x9F6E", "0x9F5B", "0x9F4C", "0x84", "0x9F7C", "0x9F63", "0x5F20", "0x9F0B",
            "0x4F", "0x50", "0x87", "0x9F12",
            "0x57", "0x80", "0x82", "0x94",
            "0x9F66"
    };

    public static String b2h(byte b) {
        return String.format("%02X ", b);
    }

    public static String b2h(byte[] arr) {
        return b2h(arr, false);
    }

    public static String b2h(byte[] arr, boolean hasSpace) {
        StringBuilder sb = new StringBuilder();
        for (byte b : arr) {
            sb.append(String.format("%02X", b));
            if (hasSpace) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    public static byte[] hex2Bytes(String hexStr) {
        hexStr = hexStr.toLowerCase();
        int length = hexStr.length();
        byte[] bytes = new byte[length >> 1];
        int index = 0;
        for (int i = 0; i < length; i++) {
            if (index > hexStr.length() - 1) return bytes;
            byte highDit = (byte) (Character.digit(hexStr.charAt(index), 16) & 0xFF);
            byte lowDit = (byte) (Character.digit(hexStr.charAt(index + 1), 16) & 0xFF);
            bytes[i] = (byte) (highDit << 4 | lowDit);
            index += 2;
        }
        return bytes;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void parseData(Map<String, TLV> map, String data) {
        parseData(map, hex2Bytes(data), false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void parseData(Map<String, TLV> map, byte[] data) {
        parseData(map, data, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void parseData(Map<String, TLV> map, byte[] data, boolean isTagMatch) {
        int i = 0;

        if (data == null || data.length <= i) {
            return;
        }

        while (i < data.length) {
            boolean isConstructed = isConstructed(data[i]);
            Tuple<String, Integer> tag = getTagName(data, i);
            i += tag.b;
            Tuple<Integer, Integer> len = getTagLen(data, i);
            i += len.b;
            TLV tlv = new TLV(
                    tag.a,
                    len.a,
                    Arrays.copyOfRange(data, i, i + len.a)
            );
            i += len.a;

            if (isConstructed) {
                parseData(map, tlv.getData());
            } else if (!tlv.getTag().isEmpty()) {
                if(!isTagMatch)
                    map.put(tlv.getTag(), tlv);
                else {
                    boolean t = Arrays.stream(tagList).anyMatch(s -> s.equals("0x" + tlv.getTag()));
                    if (t) {
                        map.put(tlv.getTag(), tlv);
                    }
                }
            }
        }
    }

    public static byte[] getData(Map<String, TLV> tags, String tag, byte[] defaultValue) {
        return tags.containsKey(tag) ? tags.get(tag).getData() : defaultValue;
    }

    public static List<TLV> parsePdolData(byte[] data) {
        List<TLV> list = new ArrayList<>();
        int i = 0;

        if (data == null || data.length <= i) {
            return list;
        }

        while (i < data.length) {
            Tuple<String, Integer> tag = getTagName(data, i);
            i += tag.b;
            Tuple<Integer, Integer> len = getTagLen(data, i);
            i += len.b;
            TLV tlv = new TLV(tag.a, len.a, null);

            list.add(tlv);
        }

        return list;
    }

    /**
     * EMV book 3  -> Annex B1
     */
    private static Tuple<String, Integer> getTagName(byte[] data, int i) {
        if (data == null || data.length <= i) return new Tuple<>("", 0);

        int tagNameLen = 1;
        if ((data[i] & 0x1F) == 0x1F) {
            tagNameLen = ((data[i + 1] & 0x80) == 0x80) ? 3 : 2;
        }

        return new Tuple<>(
                Helper.b2h(Arrays.copyOfRange(data, i, i + tagNameLen)),
                tagNameLen
        );
    }

    /**
     * EMV book 3  -> Annex B2
     */
    private static Tuple<Integer, Integer> getTagLen(byte[] data, int i) {
        if (data == null || data.length <= i) return new Tuple<>(0, 0);
        int dataLen = 0;
        int countByte;
        if ((data[i] & 0x80) == 0x80) {
            countByte = (data[i++] & 0x7F);
            byte[] lenByte = Arrays.copyOfRange(data, i, i + countByte++);
            for (byte b : lenByte) {
                dataLen = (dataLen << 8) + (b & 0xFF);
            }
        } else {
            countByte = 1;
            dataLen = data[i] & 0x7F;
        }

        return new Tuple<>(dataLen, countByte);
    }

    /**
     * is nested
     */
    private static boolean isConstructed(byte b) {
        return (b & 0x20) == 0x20;
    }
}
