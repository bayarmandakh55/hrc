package com.engui.hce_rnd;

import android.app.Application;
import android.content.Context;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class BaseApplication extends Application {

    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        try {
            PosStorage.init(appContext);
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        //dal = getDal();
    }
}