package com.engui.hce_rnd;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.engui.hce_rnd.util.Helper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CardManager {

    /**
     * Track 2 pattern
     */
    private static final Pattern TRACK2_PATTERN = Pattern.compile("([0-9]{1,19})=([0-9]{4})([0-9]{3})?(.*)");

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static List<String> cardList() {
        Gson gson = new Gson();
        String cardListStr = PosStorage.getStringFromSP(PosStorage.CARD_LIST);
        LinkedHashSet<String> cardList = gson.fromJson(cardListStr, new TypeToken<LinkedHashSet<String>>(){}.getType());
        if(cardList == null)
            cardList = new LinkedHashSet<>();
        return  cardList.stream().collect(Collectors.toList());
    }

    public static void addCard(String track2){
        Gson gson = new Gson();
        String cardListStr = PosStorage.getStringFromSP(PosStorage.CARD_LIST);
        LinkedHashSet<String> cardList = gson.fromJson(cardListStr, new TypeToken<LinkedHashSet<String>>(){}.getType());
        if(cardList == null)
            cardList = new LinkedHashSet<>();

        /*String data = Helper.hexToAscii(track2);
        Log.i("card", data);
        Matcher m = TRACK2_PATTERN.matcher(data);
        String card = "";
        // Check pattern
        if (m.find()) {
            // read card number
            card = m.group(1);
            // Read expire date
            //String month = m.group(2).substring(2,4);
            //String year = m.group(2).substring(0,2);
            //var expireDate = (month+"/"+year);
            // Read service
            //var readService = new Service(m.group(3)));
        }*/
        cardList.add(track2);
        cardListStr = gson.toJson(cardList);
        PosStorage.putStringInSP(PosStorage.CARD_LIST, cardListStr);
    }

    public static String getPan(String track2){
        String data = Helper.hexToAscii(track2);
        Log.i("pan", data);
        Matcher m = TRACK2_PATTERN.matcher(data);
        String pan = "";
        // Check pattern
        if (m.find()) {
            // read card number
            pan = m.group(1);
        }
        return pan;
    }
}
