package com.engui.hce_rnd;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.engui.hce_rnd.util.Helper;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardHolder> {

    private static final Pattern TRACK2_PATTERN = Pattern.compile("([0-9]{1,19})=([0-9]{4})([0-9]{3})?(.*)");

    private List<String> cardList;
    private Context context;
    private CardAdapterListener listener;

    public interface CardAdapterListener{
        void onCardClick(String track2);
        void onLongClick(String track2);
    }

    public CardAdapter(Context context, List<String> items){
        this.context = context;
        this.cardList = items;
    }

    public void setListener(CardAdapterListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public CardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_card, null);
        return new CardHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardHolder holder, int position) {
        if(cardList != null){

            String track2 = cardList.get(position);

            String data = Helper.hexToAscii(track2);
            Log.i("card", data);
            Matcher m = TRACK2_PATTERN.matcher(data);
            String card = "";
            // Check pattern
            if (m.find()) {
                // read card number
                card = m.group(1);
                // Read expire date
                //String month = m.group(2).substring(2,4);
                //String year = m.group(2).substring(0,2);
                //var expireDate = (month+"/"+year);
                // Read service
                //var readService = new Service(m.group(3)));
            }
            if(track2.length() > 12){
                holder.cardPer.setText(card.substring(0, 4) + "  " + card.substring(4, 8) + "  " + card.substring(8, 12) + "  " + card.substring(12, 16));
            }

            holder.card.setBackgroundResource(R.drawable.card_visa);
        }
    }

    @Override
    public int getItemCount() {
        return cardList != null ? cardList.size() : 0;
    }

    public void add(String item){
        if(cardList == null){
            cardList = new ArrayList<>();
        }
        cardList.add(item);
        notifyDataSetChanged();
    }

    public void remove(String item){
        if(cardList == null){
            cardList = new ArrayList<>();
        }
        cardList.remove(item);
        notifyDataSetChanged();
    }

    public void update(List<String> items){
        if(cardList == null){
            cardList = new ArrayList<>();
        }
        Log.d("asdfsadfasdf", " " + items.size());
        cardList.clear();
        cardList.addAll(items);
        notifyDataSetChanged();
    }

    public void clear(){
        if(cardList == null){
            cardList = new ArrayList<>();
        }
        cardList.clear();
        notifyDataSetChanged();
    }

    public class CardHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView cardPer;
//        TextView cardVisaType;
        LinearLayout card;

        public CardHolder(@NonNull View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card);
            cardPer = itemView.findViewById(R.id.card_detail);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(listener != null){
                listener.onCardClick(cardList.get(getAdapterPosition()));
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if(listener != null){
                listener.onLongClick(cardList.get(getAdapterPosition()));
            }
            return true;
        }
    }
}
