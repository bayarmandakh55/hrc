package com.engui.hce_rnd.iso7816emv;

import com.engui.hce_rnd.enums.TagTypeEnum;
import com.engui.hce_rnd.enums.TagValueTypeEnum;

public interface ITag {

	enum Class {
		UNIVERSAL, APPLICATION, CONTEXT_SPECIFIC, PRIVATE
	}

	boolean isConstructed();

	String getTagString();

	byte[] getTagBytes();

	String getName();

	String getDescription();

	default TagTypeEnum getType() {
		return null;
	}

	TagValueTypeEnum getTagValueType();

	Class getTagClass();

	int getNumTagBytes();

}
