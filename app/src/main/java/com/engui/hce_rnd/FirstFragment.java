package com.engui.hce_rnd;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.engui.hce_rnd.databinding.FragmentFirstBinding;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class FirstFragment extends Fragment implements NfcCardReaderService.ResultListener, CardAdapter.CardAdapterListener{

    private FragmentFirstBinding binding;

    private CardAdapter adapter;
    private RecyclerView recyclerView;

    private NfcCardReaderService cardReaderService;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        cardReaderService = new NfcCardReaderService(getContext(), this);
        binding = FragmentFirstBinding.inflate(inflater, container, false);

        View root = binding.getRoot();
        recyclerView = root.findViewById(R.id.card_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new CardAdapter(getActivity(), new ArrayList<>());
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);


        Gson gson = new Gson();
        String cardListStr = PosStorage.getStringFromSP(PosStorage.CARD_LIST);
        LinkedHashSet<String> cardList = gson.fromJson(cardListStr, new TypeToken<LinkedHashSet<String>>(){}.getType());
        if(cardList == null)
            cardList = new LinkedHashSet<>();

        adapter.update(cardList.stream().collect(Collectors.toList()));

        return root;

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.buttonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);*/
                cardReaderService.start();
                Snackbar.make(view, "Картаа уншуулна уу.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onCardClick(String track2) {
        Intent intent = new Intent(getActivity(), CardReadActivity.class);
        intent.putExtra(CardReadActivity.PARAM_EMV_DATA, track2);
        startActivity(intent);

        Log.i("track2", track2);
        /*Bundle bundle = new Bundle();
        bundle.putString("track2", track2);
        NavHostFragment.findNavController(FirstFragment.this)
                .navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);*/
    }

    @Override
    public void onLongClick(String emvCard) {
       /* ArrayList<EmvCard> newData = new ArrayList<>();
        newData.addAll(Auth.getCardList());
        newData.remove(emvCard);
        Auth.storeCardList(newData);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.update(Auth.getCardList());
            }
        });*/
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCardReadSuccess(String track2) {
        CardManager.addCard(track2);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.update(CardManager.cardList());
            }
        });
    }

    @Override
    public void onCardReadError(String message) {
        getActivity().runOnUiThread(() -> Toast.makeText(getContext(), message,
                Toast.LENGTH_LONG).show());
    }

    @Override
    public void onCardEmulationRead(String card) {

    }
}