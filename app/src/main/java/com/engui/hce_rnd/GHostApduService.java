package com.engui.hce_rnd;

import static com.engui.hce_rnd.CardReadActivity.PARAM_EMV_DATA;

import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.util.Log;

import com.engui.hce_rnd.model.ApduMessage;
import com.engui.hce_rnd.model.EmvCard;
import com.engui.hce_rnd.util.Helper;
import com.google.gson.Gson;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class GHostApduService extends HostApduService {

    private static final byte[] ISO7816_UNKNOWN_ERROR_RESPONSE = {
            (byte)0x6F, (byte)0x00
    };

    private static final byte[] ISO7816_FUNCTION_NOT_SUPPORTED_ERROR_RESPONSE = {
            (byte)0x6A, (byte)0x81
    };

    private static final byte[] PPSE_APDU_SELECT = {
            (byte)0x00, // CLA (class of command)
            (byte)0xA4, // INS (instruction); A4 = select
            (byte)0x04, // P1  (parameter 1)  (0x04: select by name)
            (byte)0x00, // P2  (parameter 2)
            (byte)0x0E, // LC  (length of data)  14 (0x0E) = length("2PAY.SYS.DDF01")
            // 2PAY.SYS.DDF01 (ASCII values of characters used):
            // This value requests the card or payment device to list the application
            // identifiers (AIDs) it supports in the response:
            '2', 'P', 'A', 'Y', '.', 'S', 'Y', 'S', '.', 'D', 'D', 'F', '0', '1',
            (byte)0x00 // LE   (max length of expected result, 0 implies 256)
    };

    private static final byte[] PPSE_APDU_SELECT_1 = {
            (byte)0x00, // CLA (class of command)
            (byte)0xA4, // INS (instruction); A4 = select
            (byte)0x04, // P1  (parameter 1)  (0x04: select by name)
            (byte)0x00, // P2  (parameter 2)
            (byte)0x0E, // LC  (length of data)  14 (0x0E) = length("2PAY.SYS.DDF01")
            // 2PAY.SYS.DDF01 (ASCII values of characters used):
            // This value requests the card or payment device to list the application
            // identifiers (AIDs) it supports in the response:
            '1', 'P', 'A', 'Y', '.', 'S', 'Y', 'S', '.', 'D', 'D', 'F', '0', '1',
            (byte)0x00 // LE   (max length of expected result, 0 implies 256)
    };

    private static final Map<String, String> map = new HashMap<>();

    static {
        map.put("00A4000000", "6A86");
        //map.put("00A404000E", "6F20840E315041592E5359532E4444463031A50E8801015F2D046D6E656E9F1101019000");
        map.put("00B2010C00", "702761254F07A0000000031010500A564953412044454249548701019F120A564953412044454249549000");
        map.put("00B2020C00", "6A83");
        map.put("00A404000E", "6F2F840E325041592E5359532E4444463031A51DBF0C1A61184F07A0000000031010500A564953412044454249548701019000");
        map.put("00A4040007", "6F588407A0000000031010A54D500A564953412044454249548701019F38189F66049F02069F03069F1A0295055F2A029A039C019F37045F2D046D6E656E9F1101019F120A56495341204445424954BF0C089F5A0540049604969000");
        map.put("80A8000023", "7781CA820220209408180102001005070157134207334731024911D23042214731028914731F9F100706010A03A000009F2608B5405566C8CB8C2A9F360200439F2701809F6C0238009F4B8180279EDE35CEFCBADE9CF81D5943620699A9882C45219298DFE03EA87D361A050768D0A61C5B46F35CBEFF02783D143E82E8B69A4A8F6F011C0E1EB42BA76A24C68585E4EF3E26CC08AA97E0A574ADC05CAF1FB0355372FF064355BFFE58615EECEAB9EA85C2CD284666B8C1A308AAFD33F3BDFF32EA17F8D5A4FB2F0AFF19ADBF9000");
        map.put("00B2011C00", "7081FB9081F89B75B8B6ED2067E3D24534FEDE3B4DBCA5EC9F01605A410110925A4F60553185706D1EB2551763DC017C45D81DEA3B6E8A291A0D6C8B7E759E136763A805CA445A80AC6A98612BA24C512ADD982CEE7F919C21392FC1E6F4CB7EC7E6A82DD1793BD111AE976B04958318F247B873AB064E431DE0C91399F5DD76B913F70FD397C1DD921D4714658813EABCE9D3345BBE84E8A2291A124167D9664EC25AB304BF59628D0461A6116DD7904B42004D19AE2D293D4BA7EC60999FBC2FCC3064734BE020C2BC1CA24034936A06D08DFB2809E96507E80A370B904A407A8A9358DD065B3E28AED1280CDD959448B37A9135DA0EBB7B348A4E02CD9000");
        map.put("00B2021C00", "70078F01099F3201039000");
        map.put("00B2051400", "70195F24032304305A0842073347310249115F3401015F280204969000");
        map.put("00B2061400", "7081B49F4681B0CBE0D90E3A2687B776E66A33F6ED94F95453C9A6063E3F17AC8EE296BCFB8CBC41EB5368D0573B1683B50485BA048093705AE8769A38233F677B044287BD8E5589FAE811F36AFF0AA00393C31537C596329B193F43D13C3B0C09D0C80944B8CF1654D0156595398CD055A1CFDD55B0C6C0E1832C28AE6A03C92E963E18E52924FCD74FAAC98707EC7FA274A8F7C81C06A2DD747CD6C373E0A7BA2D7B436418C684D41EEE54FE43D299B58972196C3C379000");
        map.put("00B2071400", "701F9F4701039F49039F37049F6E04207000009F4A01829F6907016192C0E238009000");
        map.put("80CA9F3600", "9F360200439000");
        map.put("80CA9F1300", "6A81");
        map.put("0084000000", "6A81");
        map.put("", "");
        map.put("", "");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle extras) {
        System.out.println(" init ");

        try {
            String track2 = PosStorage.getStringFromSP("TRACK2_");
            String emvCardStr = PosStorage.getStringFromSP(track2);

            Gson gson = new Gson();
            EmvCard card = gson.fromJson(emvCardStr, EmvCard.class);

            Log.i("apdu", Helper.b2h(commandApdu));

            byte[] command = Arrays.copyOfRange(commandApdu, 0, 4);
            if(Helper.b2h(command).equals("00A40400")){
                command = Arrays.copyOfRange(commandApdu, 0, 6);
            } else {
                command = Arrays.copyOfRange(commandApdu, 0, 4);
            }

            Log.i("apdu", "map key: " + Helper.b2h(command));

            Map<String, ApduMessage> map = card.getApduMessageMap();
            String key = Helper.b2h(command);
            ApduMessage message = map.get(key);
            if(message != null){
                ByteBuffer buffer = ByteBuffer.allocate(message.getResponseData().length + 2);
                if(message.getSw1() == 0x6A && message.getSw2() == 0x86){
                    buffer.put(message.getResponseData()).put((byte)0x90).put((byte)0x00);
                } else {
                    buffer.put(message.getResponseData()).put((byte)message.getSw1()).put((byte)message.getSw2());
                }

                Log.i("apdu get", Helper.b2h(message.getData()));

                Log.i("apdu", "response: " + Helper.b2h(buffer.array()));
                return buffer.array();
            } else {
                return ISO7816_FUNCTION_NOT_SUPPORTED_ERROR_RESPONSE;
            }
            /*String response = map.get(Helper.b2h(command));
            if(response == null)
                Log.d("empty:::", "********************************************** hooson");
            return Helper.h2b(response);*/
        }catch (Exception e){
            e.printStackTrace();
        }
        return ISO7816_UNKNOWN_ERROR_RESPONSE;
    }

    @Override
    public void onDeactivated(int reason) {
        System.out.println(" ************************************************************************************ onDeactivated");
    }
}
