package com.engui.hce_rnd;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.engui.hce_rnd.databinding.FragmentSecondBinding;
import com.engui.hce_rnd.model.EmvCard;
import com.google.gson.Gson;

public class SecondFragment extends Fragment {

    private FragmentSecondBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentSecondBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        String track2 = "";
        if(args !=null)
            track2 = args.getString("track2");

        Log.i("bundle", track2.toString());

        String emvCardStr = PosStorage.getStringFromSP(track2);

        Gson gson = new Gson();
        EmvCard card = gson.fromJson(emvCardStr, EmvCard.class);

        Intent intent = new Intent(this.getContext(), GHostApduService.class);
        intent.putExtra("ndefMessage", "success");
        intent.putExtra("track2", track2);
        getContext().startService(intent);


        binding.buttonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}