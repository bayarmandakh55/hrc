package com.engui.hce_rnd.model;

public class ApduMessage {

    private byte[] command;

    private byte[] commandEx;

    private byte[] data;

    private int lc;

    private byte[] responseData;

    private int le;

    private int sw1;

    private int sw2;

    public byte[] getCommand() {
        return command;
    }

    public void setCommand(byte[] command) {
        this.command = command;
    }

    public byte[] getCommandEx() {
        return commandEx;
    }

    public void setCommandEx(byte[] commandEx) {
        this.commandEx = commandEx;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public int getLc() {
        return lc;
    }

    public void setLc(int lc) {
        this.lc = lc;
    }

    public byte[] getResponseData() {
        return responseData;
    }

    public void setResponseData(byte[] responseData) {
        this.responseData = responseData;
    }

    public int getLe() {
        return le;
    }

    public void setLe(int le) {
        this.le = le;
    }

    public int getSw1() {
        return sw1;
    }

    public void setSw1(int sw1) {
        this.sw1 = sw1;
    }

    public int getSw2() {
        return sw2;
    }

    public void setSw2(int sw2) {
        this.sw2 = sw2;
    }

    public byte[] getSw1sw2() {
        return new byte[]{(byte)sw1, (byte)sw2};
    }

}
