package com.engui.hce_rnd.model;

import com.engui.hce_rnd.enums.EmvCardScheme;

import java.util.Map;

public class EmvCard {

    private String track2;

    private String cardholder;

    private EmvCardScheme cardScheme;

    Map<String, ApduMessage> apduMessageMap;

    public EmvCard(String track2, Map<String, ApduMessage> apduMessageMap) {
        this.track2 = track2;
        this.apduMessageMap = apduMessageMap;
    }

    public String getTrack2() {
        return track2;
    }

    public void setTrack2(String track2) {
        this.track2 = track2;
    }

    public String getCardholder() {
        return cardholder;
    }

    public void setCardholder(String cardholder) {
        this.cardholder = cardholder;
    }

    public EmvCardScheme getCardScheme() {
        return cardScheme;
    }

    public void setCardScheme(EmvCardScheme cardScheme) {
        this.cardScheme = cardScheme;
    }

    public Map<String, ApduMessage> getApduMessageMap() {
        return apduMessageMap;
    }

    public void setApduMessageMap(Map<String, ApduMessage> apduMessageMap) {
        this.apduMessageMap = apduMessageMap;
    }
}
