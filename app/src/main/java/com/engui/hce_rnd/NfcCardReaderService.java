package com.engui.hce_rnd;

import android.app.Activity;
import android.content.Context;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.engui.hce_rnd.enums.CommandEnum;
import com.engui.hce_rnd.enums.EmvCardScheme;
import com.engui.hce_rnd.enums.SwEnum;
import com.engui.hce_rnd.iso7816emv.EmvTags;
import com.engui.hce_rnd.model.ApduMessage;
import com.engui.hce_rnd.model.ApduResponse;
import com.engui.hce_rnd.model.EmvApplication;
import com.engui.hce_rnd.model.EmvCard;
import com.engui.hce_rnd.util.Helper;
import com.engui.hce_rnd.util.ResponseUtils;
import com.engui.hce_rnd.util.TLV;
import com.engui.hce_rnd.util.TLVUtils;
import com.google.gson.Gson;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class NfcCardReaderService implements NfcAdapter.ReaderCallback{

    private Context mContext;
    private ResultListener mResultListener;
    private NfcAdapter mNfcAdapter;
    private IsoDep mIsoDep;

    private static final String TAG = "qweqwe";
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyMMdd");

    private static final int READ_TIMEOUT = 3000;

    private static final int READER_FLAGS =
            NfcAdapter.FLAG_READER_NFC_A |
                    NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK |
                    NfcAdapter.FLAG_READER_NO_PLATFORM_SOUNDS;

    private Map<String, TLV> tags;
    private List<TLV> PDOL;                // Processing Options Data Object List

    private byte[] AIP = null;          // Application Interchange Profile
    private byte[] AFL = null;          // Application File Locator (identifies the files and records which are necessary for the transaction). (list of record (AEF))

    private Map<String, ApduMessage> apduMessages;

    public NfcCardReaderService(Context mContext, ResultListener mResultListener) {
        this.mContext = mContext;
        this.mResultListener=  mResultListener;
    }

    // NFC reader start
    public void start() {
        Log.d("qweqwe", "NDEF_CHECK = " + String.valueOf(NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK));
        Log.d("qweqwe", "SOUNDS = " + String.valueOf(NfcAdapter.FLAG_READER_NO_PLATFORM_SOUNDS));
        mNfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
        if (mNfcAdapter == null) {
            Toast.makeText(mContext, "NFC дэмждэггүй төхөөрөмж байна!!!", Toast.LENGTH_LONG).show();
        }
        if (!mNfcAdapter.isEnabled()) {
            System.out.println("NFC асаана уу");
            Toast.makeText(mContext, "NFC асаана уу",
                    Toast.LENGTH_LONG).show();
        } else {
            System.out.println("enableReaderMode");
            mNfcAdapter.enableReaderMode((Activity) mContext, this, READER_FLAGS, null);

            AIP = null;
            AFL = null;

            tags = new HashMap<>();
            tags.put("5F2A", new TLV(EmvTags.TRANSACTION_CURRENCY_CODE.getTagString().toUpperCase(), 2, Helper.h2b("0496")));
            tags.put("9F1A", new TLV(EmvTags.TERMINAL_COUNTRY_CODE.getTagString().toUpperCase(), 2, Helper.h2b("0496")));
            tags.put("95", new TLV(EmvTags.TERMINAL_VERIFICATION_RESULTS.getTagString(), 5, Helper.h2b("0880048000"))); // DDA - 0880048000, SDA - 0480048000
            tags.put("9F1E", new TLV(EmvTags.INTERFACE_DEVICE_SERIAL_NUMBER.getTagString().toUpperCase(), 8, Helper.h2b("3030303030393035"))); //Interface Device (IFD) Serial Number
            tags.put("9F33", new TLV(EmvTags.TERMINAL_CAPABILITIES.getTagString().toUpperCase(), 3, Helper.h2b("E0F8C8")));     // Terminal Capabilities
            tags.put("9F34", new TLV(EmvTags.CVM_RESULTS.getTagString().toUpperCase(), 3, Helper.h2b("420300")));     // Cardholder Verification Method (CVM) Results  -  Indicates the results of the last CVM performed
            tags.put("9F35", new TLV(EmvTags.TERMINAL_TYPE.getTagString().toUpperCase(), 1, Helper.h2b("22")));     // Terminal type
            tags.put("5F36", new TLV(EmvTags.APP_TRANSACTION_COUNTER.getTagString().toUpperCase(), 1, Helper.h2b("00")));

            tags.put("9F41", new TLV("9F41", 4, Helper.h2b(getTransactionCounter())));
            tags.put("9B", new TLV(EmvTags.TRANSACTION_STATUS_INFORMATION.getTagString().toUpperCase(), 2, Helper.h2b("E800")));
            tags.put("9C", new TLV(EmvTags.TRANSACTION_TYPE.getTagString().toUpperCase(), 1, Helper.h2b("00")));
            tags.put("9F03", new TLV(EmvTags.AMOUNT_OTHER_NUMERIC.getTagString().toUpperCase(), 6, Helper.h2b("000000000000")));
            tags.put("9F08", new TLV(EmvTags.APP_TRANSACTION_COUNTER.getTagString().toUpperCase(), 1, Helper.h2b("00")));
            // (TTQ) 07010103A0000001
            tags.put("9F66", new TLV(EmvTags.TERMINAL_TRANSACTION_QUALIFIERS.getTagString().toUpperCase(), 4, Helper.h2b("B6C0C080"))); // Terminal Transaction
            //tags.put("9F66", new TLV(EmvTags.TERMINAL_TRANSACTION_QUALIFIERS.getTagString().toUpperCase(), 4, Helper.h2b("26C00080"))); // Terminal Transaction Qualifiers (TTQ)

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onTagDiscovered(Tag tag) {
        mIsoDep = IsoDep.get(tag);
        try {
            mIsoDep.connect();
            mIsoDep.setTimeout(READ_TIMEOUT);

            apduMessages = new HashMap<>();

            // $$ [Step 1] SELECT FILE Master File (if available)
            byte[] data = new byte[]{};
            ApduMessage message = commandApdu(CommandEnum.SELECT_MASTER, data, 0);
            Log.d("step", "1: " + String.format("data: %s\n sw1: %h, sw2: %h", Helper.b2h(message.getResponseData()), message.getSw1(), message.getSw2()));

            //-------------------------------------------------------------------------------------------
            // $$ [Step 2] SELECT FILE 1PAY.SYS.DDF01 to get the PSE directory
            //  # select PSE
            // '1PAY.SYS.DDF01'
            // 00A404000E315041592E5359532E444446303100
            data = new byte[]{(byte) 0x31, (byte) 0x50, (byte) 0x41, (byte) 0x59, (byte) 0x2E, (byte) 0x53, (byte) 0x59, (byte) 0x53, (byte) 0x2E, (byte) 0x44, (byte) 0x44, (byte) 0x46, (byte) 0x30, (byte) 0x31};
            message = commandApdu(CommandEnum.SELECT, data, 0);
            Log.d("step", "2: " + String.format("data: %s\n sw1: %h", Helper.b2h(message.getResponseData()), message.getSw1()));

            byte sfi = 0x01;   // tag - 88
            if (message.getSw1() == 0x90) {
                TLVUtils.parseData(tags, message.getResponseData());

                TLV tlv = tags.get("88");
                if (tlv != null && tlv.getLen() > 0) {
                    sfi = tlv.getData()[0];
                }
            }

            //-------------------------------------------------------------------------------------------
            // $$ [Step 3] Send READ RECORD to read all records in SFI 1
            List<EmvApplication> applications = new ArrayList<>();
            for (int recNo = 1; recNo <= 4; recNo++) {
                // 00B2010C00
                data = new byte[]{};
                message = commandApdu(CommandEnum.READ_RECORD,  recNo, ((sfi << 3) | 0x04), data.length, data, 0);
                Log.d("step", "3: " + String.format("data: %s\n sw1: %h", Helper.b2h(message.getResponseData()), message.getSw1()));

                Map<String, TLV> dataTags = new HashMap<>();
                TLVUtils.parseData(dataTags, message.getResponseData());

                TLV aid = dataTags.get("4F");
                TLV label = dataTags.get("50");
                TLV priority = dataTags.get("87");
                TLV preferredName = dataTags.get("9F12");

                if (aid != null) {
                    applications.add(new EmvApplication(
                            aid.getData(),
                            (label != null) ? Arrays.toString(label.getData()) : "",
                            priority != null ? (int) priority.getData()[0] : 0,
                            preferredName != null ? Arrays.toString(preferredName.getData()) : ""
                    ));
                }

                // "A0000000031010"
                if (message.getSw1() != 0x90) {
                    Log.d("qweqwe_", "BREAK");
                    break;
                }

            }

            EmvApplication application = null;
            if(applications.size() > 0) {
                application = applications.get(0);
                for (EmvApplication app : applications) {
                    if (app.getPriority() > application.getPriority()) {
                        application = app;
                    }
                }
            }
            // select app PPSE '2PAY.SYS.DDF01' - contactless учираас PPSE ашигласан байгаа
            // 00A404000E325041592E5359532E444446303100
            data = new byte[]{(byte) 0x32, (byte) 0x50, (byte) 0x41, (byte) 0x59, (byte) 0x2E, (byte) 0x53, (byte) 0x59, (byte) 0x53, (byte) 0x2E, (byte) 0x44, (byte) 0x44, (byte) 0x46, (byte) 0x30, (byte) 0x31};
            message = commandApdu(CommandEnum.SELECT, data, 0);
            Log.d("step", "2: " + String.format("data: %s\n sw1: %h", Helper.b2h(message.getResponseData()), message.getSw1()));

            if (ResponseUtils.isSucceed(message.getSw1sw2())) {
                TLVUtils.parseData(tags, message.getResponseData());
                Map<String, TLV> dataTags = new HashMap<>();
                TLVUtils.parseData(dataTags, message.getResponseData());

                TLV aid = dataTags.get(EmvTags.AID_CARD.getTagString().toUpperCase()); //Application Identifier (ADF Name)
                TLV label = dataTags.get(EmvTags.APPLICATION_LABEL.getTagString()); //Application Label
                TLV priority = dataTags.get(EmvTags.APPLICATION_PRIORITY_INDICATOR.getTagString()); //Application Priority Indicator
                TLV preferredName = dataTags.get(EmvTags.APP_PREFERRED_NAME.getTagString().toUpperCase()); //Application Preferred Name

                if(aid != null) {
                    application = new EmvApplication(
                            aid.getData(),
                            (label != null) ? Arrays.toString(label.getData()) : "",
                            priority != null ? (int) priority.getData()[0] : 0,
                            preferredName != null ? Arrays.toString(preferredName.getData()) : ""
                    );
                }
            } else {
                mResultListener.onCardReadError("select env: " + SwEnum.getSW(message.getSw1sw2()).getDetail());
                mIsoDep.close();
                return;
            }

            String aid = Helper.b2h(application.getAID());
            EmvCardScheme cardScheme = EmvCardScheme.getCardTypeByAid(aid);
            Log.i("card type",  cardScheme.getName() + " : " + aid);

            if(EmvCardScheme.VISA.equals(cardScheme)){
                String[] tagsPayWave = {"DF8124", "DF8125", "DF8126"};
                String[] valuesPayWave = {"999999999999", "999999999999", "000000000000"};
                AtomicInteger i = new AtomicInteger();
                Arrays.stream(tagsPayWave).forEach(tagPayWave ->{
                    String v =valuesPayWave[i.get()];
                    tags.put(tagPayWave, new TLV(tagPayWave, v.length() / 2, Helper.h2b(v)));
                    i.getAndIncrement();
                });
            } else if(EmvCardScheme.MASTER_CARD.equals(cardScheme)) {
                String[] tagsPayPass = {
                        "DF8117", "DF8118", "DF8119", "DF811B", "DF811D",
                        "DF811E", "DF811F", "DF8120", "DF8121", "DF8122",
                        "DF8123", "DF8124", "DF8125", "DF812C"
                };
                String[] valuesPayPass = {
                        "E0", "F8", "F8", "30", "02",
                        "00", "E8", "F45084800C", "0000000000", "F45084800C",
                        "000000000000", "999999999999", "999999999999", "00"
                };
                AtomicInteger i = new AtomicInteger();
                Arrays.stream(tagsPayPass).forEach(tagPayPass ->{
                    String v =valuesPayPass[i.get()];
                    tags.put(tagPayPass, new TLV(tagPayPass, v.length() / 2, Helper.h2b(v)));
                    i.getAndIncrement();
                });
            }

            // $$ [Step 6] Select(AID)--
            // byte[] arr3 = new byte[]{(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00,(byte) 0x07,(byte) 0xA0, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x03, (byte) 0x10, (byte) 0x10,(byte) 0x00};
            // 00A4040007 A0000000031010 006F3A8407A0000000031010A52F500A564953412044454249548701015F2D046D6E656E9F1101019F120A56495341204445424954BF0C055F55024D4E9000
            //command = new byte[]{(byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00};
            data = application.getAID();
            message = commandApdu(CommandEnum.SELECT, data, 0);
            Log.d("step", "6: " + String.format("data: %s\n sw1: %h", Helper.b2h(message.getResponseData()), message.getSw1()));
            if (ResponseUtils.isSucceed(message.getSw1sw2())) {
                TLVUtils.parseData(tags, message.getResponseData());
            } else {
                mResultListener.onCardReadError("select app: " + SwEnum.getSW(message.getSw1sw2()).getDetail());
                mIsoDep.close();
                return;
            }

            Integer amount = 1000;
            String s = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
            String a = "000000000000" + amount;
            String amount1 = a.substring(a.length() - 12);

            byte[] t9F37 = getData("9F37");
            if(t9F37 == null || t9F37.length <= 0) {
                t9F37 = Helper.h2b(randomStr(8));
                tags.put("9F37", new TLV(EmvTags.UNPREDICTABLE_NUMBER.getTagString().toUpperCase(), t9F37.length, t9F37));
            }
            tags.put("9A", new TLV(EmvTags.TRANSACTION_DATE.getTagString().toUpperCase(), 3, Helper.h2b(s)));
            tags.put("9F02", new TLV(EmvTags.AMOUNT_AUTHORISED_NUMERIC.getTagString().toUpperCase(), 6, Helper.h2b(amount1)));
            tags.put("9F06", new TLV(EmvTags.AID_TERMINAL.getTagString().toUpperCase(), application.getAID().length, application.getAID()));

            PDOL = getPdol();
            byte[] pdolData = preparePdolData();
            Log.d(TAG, "PDOL-DATA: " + Helper.b2h(pdolData));
            
            //-------------------------------------------------------------------------------------------
            // $$ [Step 7] Send GET PROCESSING OPTIONS command
            // GET PROCESSING OPTIONS - initialize transaction:
            /** 9F38 - Processing Options Data Object List (PDOL) */
            // 80A8000002830000
            data = pdolData;
            message = commandApdu(CommandEnum.GPO, data, 0);
            Log.d("step", "7: " + String.format("data: %s\n sw1: %h, sw2: %h", Helper.b2h(message.getResponseData()), message.getSw1(), message.getSw2()));

            if (ResponseUtils.isSucceed(message.getSw1sw2())) {
                byte[] res = message.getResponseData();
                byte format = res[0];
                TLVUtils.parseData(tags, res);

                if (format == (byte) 0x80) { //Response Message Template Format 1
                    byte[] data80 = getData(EmvTags.RESPONSE_MESSAGE_TEMPLATE_1.getTagString(), new byte[]{});
                    AIP = new byte[]{data80[0], data80[1]};
                    AFL = new byte[data80.length - 2];
                    System.arraycopy(data80, 2, AFL, 0, data80.length - 2);
                    tags.put("82", new TLV(EmvTags.APPLICATION_INTERCHANGE_PROFILE.getTagString(), AIP.length, AIP));
                    tags.put("94", new TLV(EmvTags.APPLICATION_FILE_LOCATOR.getTagString(), AFL.length, AFL));
                } else if (format == (byte) 0x77) { //Template, Response Message Format 2
                    AIP = getData("82", new byte[]{});
                    AFL = getData("94", new byte[]{});
                }

                int i = 0;
                while (i < AFL.length - 1) {
                    sfi = AFL[i];
                    int startRecord = AFL[i + 1];
                    int endRecord = AFL[i + 2];
                    //int countSDA = AFL[i + 3];      // TODO: ?? Number of records included in data authentication

                    // check all records
                    for (int index = startRecord; index <= endRecord; index++) {
                        //command = new byte[]{(byte) 0x00, (byte) 0xB2, (byte) index, (byte)(sfi & 248 | 0x04)};
                        data = new byte[]{};
                        message = commandApdu(CommandEnum.READ_RECORD, index, (sfi & 248 | 0x04), data.length, data, 0);
                        if (ResponseUtils.isEquals(message.getSw1sw2(), SwEnum.SW_6C)) {
                            //command = new byte[]{(byte) 0x00, (byte) 0xB2, (byte) index, (byte)(sfi << 248 | 0x04)};
                            data = new byte[]{};
                            message = commandApdu(CommandEnum.READ_RECORD, index, (sfi & 248 | 0x04), data.length, data, message.getSw2());
                        }
                        // Extract card data
                        if (ResponseUtils.isSucceed(message.getSw1sw2())) {
                            TLVUtils.parseData(tags, message.getResponseData());
                        }
                    }
                    i += 4;
                }
            }else {
                mResultListener.onCardReadError("gpo: " + SwEnum.getSW(message.getSw1sw2()).getDetail());
                mIsoDep.close();
                return;
            }

            //get Track3

            getTrack3(application);

            // $$ [Step 13] Send GET DATA command to find the Unpredictable Number
            data = new byte[]{};
            message = commandApdu(CommandEnum.GET_DATA, 0x9F, 0x37, data.length, data, 0);  // 9F370204BD6A9000
            Log.d("step", "12: " + String.format("data: %s\n sw1: %h, sw2: %h", Helper.b2h(message.getResponseData()), message.getSw1(), message.getSw2()));
            if (message.getSw1() == 0x90) {
                TLVUtils.parseData(tags, message.getResponseData());
            }

            if(EmvCardScheme.MASTER_CARD.equals(cardScheme)) {
                // $$ [Step 13] Send GET DATA command to find the Application Transaction Counter (ATC)
                data = new byte[]{};
                message = commandApdu(CommandEnum.GET_DATA, 0x9F, 0x36, data.length, data, 0);  // 9F360204BD9000
                Log.d("step", "13: " + String.format("data: %s\n sw1: %h, sw2: %h", Helper.b2h(message.getResponseData()), message.getSw1(), message.getSw2()));
                if (message.getSw1() == 0x90) {
                    TLVUtils.parseData(tags, message.getResponseData());
                }
                // $$ [Step 14] Send GET DATA command to find the Last Online ATC Register
                data = new byte[]{};
                message = commandApdu(CommandEnum.GET_DATA, 0x9F, 0x13, data.length, data, 0);  // 9F130204B49000
                Log.d("step", "14: " + String.format("data: %s\n sw1: %h, sw2: %h", Helper.b2h(message.getResponseData()), message.getSw1(), message.getSw2()));

                // $$ [Step 15] Send GET DATA command to find the PIN Try Counter
                data = new byte[]{};
                message = commandApdu(CommandEnum.GET_DATA, 0x9F, 0x17, data.length, data, 0);
                Log.d("step", "15: " + String.format("data: %s\n sw1: %h, sw2: %h", Helper.b2h(message.getResponseData()), message.getSw1(), message.getSw2()));

                // $$ [Step 16] Send GET DATA command to find the Log Format
                data = new byte[]{};
                message = commandApdu(CommandEnum.GET_DATA, 0x9F, 0x4F, data.length, data, 0);
                Log.d("step", "16: " + String.format("data: %s\n sw1: %h, sw2: %h", Helper.b2h(message.getResponseData()), message.getSw1(), message.getSw2()));
            }

            tags.forEach((tagStr, tlv) -> {
                Log.i("tags", tagStr + " : " + Helper.b2h(tlv.getData()));
            });

            EmvCard card = new EmvCard(getTrack2(), apduMessages);
            card.setCardholder(getCardholder());
            card.setCardScheme(cardScheme);

            byte[] b = getData(EmvTags.APP_VERSION_NUMBER_CARD.getTagString().toUpperCase());

            if (b != null) {
                String name = new String(b);
                System.out.println(name);
            }

            System.out.println("get track3 --------------------------------------------------");
            System.out.println(getTrack3Cus("200"));
            System.out.println("get track3 -------------------------------------------------- end");

            Log.i("cardholder", getCardholder() + "");

            Gson gson = new Gson();
            String jsonStr = gson.toJson(card);
            PosStorage.putStringInSP(card.getTrack2(), jsonStr);

            mResultListener.onCardReadSuccess(card.getTrack2());
            mIsoDep.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 9F66049F02069F03069F1A0295055F2A029A039C019F3704

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void getTrack3(EmvApplication application) throws IOException {
        List<TLV> cdol1 = getCdol1();
        Log.d(TAG, "Generate AC: len = " + cdol1.size());
        Log.d(TAG, "CDOL-1: " + Helper.b2h(getData("8C", new byte[]{})));
        Log.d(TAG, "CDOL-2: " + Helper.b2h(getData("8D", new byte[]{})));

        byte[] cdolData = prepareCdolData();
        Log.i("cdol", Helper.b2h(cdolData));

        //byte p1 = (byte) 0x40; // offline transaction
        //p1 = (byte) 0x80; // online transaction
        byte[] data = cdolData;
        System.out.println("byte[] data ======================================");
        System.out.println(data);
        ApduMessage message = commandApdu(CommandEnum.GENERATE_AC, data, 0);
        Log.d("step", "Generate AC 90: " + String.format("data: %s\n sw1: %h, sw2: %h", Helper.b2h(message.getResponseData()), message.getSw1(), message.getSw2()));

        if (ResponseUtils.isSucceed(message.getSw1sw2())) {
            TLVUtils.parseData(tags, message.getResponseData());
            byte format = message.getResponseData()[0];
            TLV tlv = format == (byte) 0x80 ? tags.get("80") : tags.get("77");
            Log.d("xxxaa 9F26", "==> : if iishee orsoon");
            if (tlv != null) {
                data = tlv.getData();
                int i = 0;
                tags.put("9F27", new TLV(EmvTags.CRYPTOGRAM_INFORMATION_DATA.getTagString().toUpperCase(), 1, new byte[]{data[0]}));
                tags.put("9F36", new TLV(EmvTags.APP_TRANSACTION_COUNTER.getTagString().toUpperCase(), 2, new byte[]{data[1], data[2]}));
                Log.d("xxxaa 9F26", "==> : " + Helper.b2h(data[1]));
                byte[] ac = new byte[8];
                System.arraycopy(data, 3, ac, 0, 8);
                tags.put("9F26", new TLV(EmvTags.APP_CRYPTOGRAM.getTagString().toUpperCase(), 2, ac));

                if (format == (byte) 0x80 && data.length > 11) {
                    int len = data.length - 11;
                    byte[] iad = new byte[len];
                    System.arraycopy(data, 11, iad, 0, len);
                    tags.put("9F10", new TLV(EmvTags.ISSUER_APPLICATION_DATA.getTagString().toUpperCase(), len, iad));
                }
            }
        }
    }

    public String getTrack3Cus(String amount) {
        String[] tagList = {
                "5F20", "5F2A", "5F34",
                "82", "84",
                "95", "9A", "9B", "9C",
                "9F02", "9F03", "9F06", "9F08", "9F0B",
                "9F10", "9F1A", "9F1E",
                "9F26", "9F27", "9F33", "9F34", "9F35", "9F36", "9F37", "9F41",
                "9F4B", "9F4C",
                "9F63", "9F6E",
                "9F7C"};
        // 9F5B

        String s = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
        String a = "000000000000" + amount;
        String amount1 = a.substring(a.length() - 12);
        Random random = new Random();
        byte[] t9F37 = new byte[]{
                (byte) random.nextInt(256),
                (byte) random.nextInt(256),
                (byte) random.nextInt(256),
                (byte) random.nextInt(256),
        };
        tags.put("9A", new TLV("9A", 3, Helper.h2b(s)));
        tags.put("9F02", new TLV("9F02", 6, Helper.h2b(amount1)));
//        tags.put("9F06", new TLV("9F06", application.getAID().length, application.getAID()));
        tags.put("9F37", new TLV("9F37", t9F37.length, t9F37));

        Map<String, TLV> map = new LinkedHashMap<>();
        for (String key : tagList) {
            //Log.d("qweqwe", "==> " + key + ": " + tags.get(key));
            if (tags.containsKey(key)) {
                map.put(key, tags.get(key));
            } else {
                map.put(key, new TLV(key, 0, new byte[]{}));
            }
        }

        Log.d(TAG, "Before ber tlv: " + map);
        String result = "";
        for (String key : tagList) {
            Log.d("qweqwe", "==> " + key + ": " + Helper.b2h(map.get(key).getData()) + " : len = " +map.get(key).getLen());
            result += key;
            if(map.get(key).getLen() == 0){
                result += "00";
            }else {
                result += map.get(key).getLen() > 10 ? "0" +map.get(key).getLen() : map.get(key).getLen() + "";
            }
            result += Helper.b2h(map.get(key).getData());
        }

        return Helper.formatBerTlv(map);
//        return result;
    }



    private String getTransactionCounter() {
        PosStorage.putIntegerInSP(
                PosStorage.TRANSACTION_COUNTER,
                PosStorage.getIntegerFromSP(PosStorage.TRANSACTION_COUNTER) + 1
        );
        String s = "00000000" + PosStorage.getIntegerFromSP(PosStorage.TRANSACTION_COUNTER);

        return s.substring(s.length() - 8);
    }

    private List<TLV> getPdol() {
        return TLVUtils.parsePdolData(getData("9F38", new byte[]{}));
    }

    private List<TLV> getCdol1() {
        return TLVUtils.parsePdolData(getData("8C", new byte[]{}));
    }

    private byte[] getData(String tag) {
        return getData(tag, null);
    }

    private byte[] getData(String tag, byte[] defaultValue) {
        return tags.containsKey(tag) ? tags.get(tag).getData() : defaultValue;
    }

    public String getTrack2() {
        byte[] b = getData("57");
        String s = b != null ? Helper.b2h(b) : "";
        int i = s.indexOf('D');

        String s2;
        if ((i + 18) <= s.length()) {
            s2 = s.substring(0, i) + "=" + s.substring(i + 1, i + 18);
        } else {
            s2 = s.replace("D", "=");
        }

        return Helper.b2h(s2.getBytes());
    }

    public String getCardholder() {
        byte[] b = getData(EmvTags.CARDHOLDER_NAME.getTagString().toUpperCase());
        if (b != null) {
            String[] name = new String(b).trim().split("/");
            if (name != null && name.length == 2) {
                String firstname = name[0].trim();
                String lastname = name[1].trim();
                return lastname + " " + firstname;
            } else if (name != null && name.length == 1){
                return name[0].trim();
            }
        }
        return null;
    }

    private byte[] preparePdolData() {
        byte[] pdolData;
        if (PDOL != null && PDOL.size() > 0) {
            int len = 0;
            for (TLV tlv : PDOL) len += tlv.getLen();
            ByteBuffer buffer = ByteBuffer.allocate(len + 2);
            buffer.put((byte) 0x83).put((byte) len);

            for (TLV tlv : PDOL) {
                byte[] bytes = new byte[tlv.getLen()];
                TLV t = tags.get(tlv.getTag());
                if (t != null && t.getData() != null && t.getLen() == tlv.getLen()) {
                    System.arraycopy(t.getData(), 0, bytes, 0, tlv.getLen());
                } else {
                    Arrays.fill(bytes, (byte) 0x00);
                }
                buffer.put(bytes);
            }
            pdolData = buffer.array();
        } else {
            return new byte[]{(byte) 0x83, (byte) 0x00};
        }

        return pdolData;
    }

    private String randomStr(int len){
        Random obj = new Random();
        int rand_num = obj.nextInt(0xffffff + 1);
        // format it as hexadecimal string and print
        return String.format("%0" + len + "x", rand_num).toUpperCase(Locale.ROOT);
    }

    private byte[] prepareCdolData() {
        byte[] cdolData;
        List<TLV> cdol = getCdol1();
        if (cdol.size() > 0) {
            int len = 0;
            for (TLV tlv : cdol) len += tlv.getLen();
            ByteBuffer buffer = ByteBuffer.allocate(len);

            for (TLV tlv : cdol) {
                byte[] bytes = new byte[tlv.getLen()];
                TLV t = tags.get(tlv.getTag());
                if (t != null && t.getData() != null && t.getLen() == tlv.getLen()) {
                    System.arraycopy(t.getData(), 0, bytes, 0, tlv.getLen());
                } else {
                    Arrays.fill(bytes, (byte) 0x00);
                }
                buffer.put(bytes);
            }
            cdolData = buffer.array();
        } else {
            return new byte[]{};
        }

        return cdolData;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private ApduMessage commandApdu(CommandEnum command, byte[] data, int le) throws IOException {
        return commandApdu(command, command.getP1(), command.getP2(), data.length, data, le);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private ApduMessage commandApdu(CommandEnum command, int p1, int p2, int lc, byte[] data, int le) throws IOException {
        byte[] commands = new byte[]{(byte)command.getCla(), (byte)command.getIns(), (byte)p1, (byte)p2};

        ApduMessage message = new ApduMessage();
        message.setCommand(commands);
        message.setData(data);
        message.setLc(lc);
        message.setLe(le);

        ByteBuffer buffer;
        if(message.getLc() > 0) {
            buffer = ByteBuffer.allocate(message.getCommand().length + message.getLc() + 2);
            buffer.put(message.getCommand())
                    .put((byte) message.getLc())
                    .put(message.getData())
                    .put((byte) message.getLe());
        } else {
            buffer = ByteBuffer.allocate(message.getCommand().length + 1);
            buffer.put(message.getCommand())
                    .put((byte) message.getLe());
        }

        byte[] response = mIsoDep.transceive(buffer.array());
        ApduResponse apduResponse = new ApduResponse(response);

        if(apduResponse.getSW1() == 0x90) {
            switch (Helper.b2h(commands)) {
                case "80A80000":
                case "80AE8000": {
                    byte[] b = new byte[apduResponse.getData().length - 2];
                    System.arraycopy(apduResponse.getData(), 2, b, 0, apduResponse.getData().length - 2);
                    int i1 = apduResponse.getData()[0];
                    int i2 = apduResponse.getData()[1] + 7;

                    Log.i("command A8", "{\"" +Helper.b2h(message.getCommand()) + ": \"" + Helper.b2h(b) + "\"}");
                    Log.i("command A81", "{\"" +Helper.b2h(message.getCommand()) + ": \"" + Helper.b2h(apduResponse.getData()) + "\"}");

                    ByteBuffer b1 = ByteBuffer.allocate(apduResponse.getData().length + 7);
                    b1.put((byte)i1).put((byte)i2).put(b).put((byte)0x9F).put((byte)0x37).put((byte)0x04).
                            put(getData("9F37"));

                    message.setResponseData(b1.array());
                    break;
                }
                default: {
                    message.setResponseData(apduResponse.getData());
                    break;
                }
            }
        } else {
            message.setResponseData(apduResponse.getData());
        }

        message.setSw1(apduResponse.getSW1());
        message.setSw2(apduResponse.getSW2());

        if(Helper.b2h(commands).equals("00A40400")) {
            buffer = ByteBuffer.allocate(message.getCommand().length + 2);
            buffer.put(message.getCommand())
                .put((byte)message.getLc())
                .put(data[0]);
        } else {
            buffer = ByteBuffer.allocate(message.getCommand().length);
            buffer.put(message.getCommand());
        }
        Log.i("command", "{\"" +Helper.b2h(message.getCommand()) + ": \"" + Helper.b2h(message.getResponseData()) + "\"}");
        message.setCommandEx(buffer.array());

        String commandStr = Helper.b2h(buffer.array());
        if("80AE8000".equals(commandStr))
            apduMessages.put("80AE9000", message);
        apduMessages.put(commandStr, message);
        return message;
    }

    public interface ResultListener {
        void onCardReadSuccess(String track2);
        void onCardReadError(String message);
        void onCardEmulationRead(String card);
    }
}