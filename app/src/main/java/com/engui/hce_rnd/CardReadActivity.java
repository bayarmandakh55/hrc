package com.engui.hce_rnd;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class CardReadActivity extends AppCompatActivity {
    public static String PARAM_EMV_DATA = "track2";
    private String track2;
    private TextView txt;
    private ImageView imageView;
    public static String cardPan;
    public static String cardTrack2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_reader);

        if(getIntent() != null){
            track2 = (String) getIntent().getSerializableExtra(PARAM_EMV_DATA);
        }
        txt = findViewById(R.id.card_number);
        imageView = findViewById(R.id.barcode);
//        txtDetial = findViewById(R.id.card_detail_info);


        txt.setText("" + CardManager.getPan(track2));

        //Log.d("asdfsadfs", "" + emvCard.getCardNumber());

        PosStorage.putStringInSP("TRACK2_", track2);

        Intent intent = new Intent(this, GHostApduService.class);
        startService(intent);
    }


}
